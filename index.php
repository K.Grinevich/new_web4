<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['radio-kon'] = !empty($_COOKIE['radio-kon_error']);
  $errors['radio-pol'] = !empty($_COOKIE['radio-pol_error']);
  $errors['sp-sp'] = !empty($_COOKIE['sp-sp_error']);
  $errors['ok'] = !empty($_COOKIE['ok_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя корректно.</div>';
  }
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните email корректно.</div>';
  }

  if ($errors['radio-kon']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('radio-kon_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Укажите количество ваших конечностей.</div>';
  }

  if ($errors['radio-pol']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('radio-pol_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Укажите свой пол.</div>';
  }

  if ($errors['sp-sp']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sp-sp_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Укажите свои суперспособности.</div>';
  }

  if ($errors['biography']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('biography_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните информацию о себе корректно.</div>';
  }

  if ($errors['ok']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('ok_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Поставьте галочку.</div>';
  }

  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['yob'] = empty($_COOKIE['yob_value']) ? '' : $_COOKIE['yob_value'];
  $values['radio-pol'] = empty($_COOKIE['radio-pol_value']) ? '' : $_COOKIE['radio-pol_value'];
  $values['radio-kon'] = empty($_COOKIE['radio-kon_value']) ? '' : $_COOKIE['radio-kon_value'];
  $values['sp-sp'] = empty($_COOKIE['sp-sp_value']) ? '' : $_COOKIE['sp-sp_value'];
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];





  

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  setlocale(LC_ALL, "ru_RU.UTF-8");
  $errors = FALSE;
  if (empty($_POST['fio']) || preg_match('/[^(\x7F-\xFF)|(\s)]/', $_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }


  if (empty($_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['radio-pol'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('radio-pol_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('radio-pol_value', $_POST['radio-pol'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['radio-kon'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('radio-kon_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('radio-kon_value', $_POST['radio-kon'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['sp-sp'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('sp-sp_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    $sp = implode('',$_POST['sp-sp']);
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('sp-sp_value', $sp, time() + 30 * 24 * 60 * 60);
  }


  if (empty($_POST['biography'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['ok'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('ok_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

  setcookie('yob_value', $_POST['yob'], time() + 30 * 24 * 60 * 60);
  
// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('radio-pol_error', '', 100000);
    setcookie('radio-kon_error', '', 100000);
    setcookie('sp-sp_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('ok_error', '', 100000);
    
    // TODO: тут необходимо удалить остальные Cookies.
  }

  // Сохранение в XML-документ.
$user = 'u26391';
$pass = '7979685';
$db = new PDO('mysql:host=localhost;dbname=u26391', $user, $pass, array(PDO::ATTR_PERSISTENT => true));



try {
  $str = implode(',',$_POST['sp-sp']);
  
  $stmt = $db->prepare("INSERT INTO application2 SET fio = ?, email = ?, yob = ?, pol = ?, limb = ?, biography = ?");
  $stmt -> execute([$_POST['fio'],$_POST['email'],$_POST['yob'],$_POST['radio-pol'],$_POST['radio-kon'],$_POST['biography']]);
  
  $arr = explode(',',$str);
  $stmt = $db->prepare("INSERT INTO spw SET id = ?, nom_spw = ?");
  foreach($arr as $el){
    $link = mysqli_connect('localhost', $user, $pass, 'u26391');
    $mail = $_POST['email'];
    $result = mysqli_query($link,"SELECT id FROM application2 WHERE email = '$mail'");
    $idd = mysqli_fetch_row($result);
    $id = (int)$idd[0];
    $stmt -> execute([$id,$el]);
  }
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
